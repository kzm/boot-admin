# boot-admin

#### 介绍
java 实践

#### 进度

2022.1.15 集成 `mybatis-plus-generator`、`freemarker`

- 实现代码生成，模板自定义
- 代码结构调整，简单化
- 数据库自动填充，配合代码生成，禁用 `controller` 生成
- 异常统一封装处理

2022.1.14 集成 `mybatis plus`、`spring doc`、`lombok`

2022.1.13 集成 `log4j2`、`fastjson`，`openapi` 替代 `swagger`

2022.1.16 实现 `JWT`

2022.1.21 集成 `Redis`

2022.7.9 实现七牛上传