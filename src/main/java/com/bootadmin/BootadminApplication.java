package com.bootadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootadminApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootadminApplication.class, args);
    }

}
