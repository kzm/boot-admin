package com.bootadmin.core.dto;

import com.bootadmin.core.utils.JsonUtils;
import com.bootadmin.core.utils.StringUtils;

import java.io.Serializable;

/**
 * @author kongzm
 * @description: BaseDTO
 * @date 2022/1/13 10:17
 */
public class BaseDTO implements DTO, Serializable {
    @Override
    public String toString() {
        return StringUtils.toObjectString(this);
    }

    @Override
    public String toJsonString() {
        return JsonUtils.toJsonString(this);
    }
}
