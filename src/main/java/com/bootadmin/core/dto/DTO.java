package com.bootadmin.core.dto;

public interface DTO {
    public  String toString();
    public  String toJsonString();
}
