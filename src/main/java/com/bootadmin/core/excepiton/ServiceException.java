package com.bootadmin.core.excepiton;

public class ServiceException extends BaseRuntimeException
{
	private static final long serialVersionUID = 1L;

	public ServiceException(Integer code, String msg)
	{
		super(code, msg);
	}
}
