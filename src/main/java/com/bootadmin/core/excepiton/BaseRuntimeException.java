package com.bootadmin.core.excepiton;

/**
 * Description: 异常基类
 */
public class BaseRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    protected int code;

    protected String msg;

    public BaseRuntimeException(int code, String msgFormat) {
        this.code = code;
        this.msg = msgFormat;
    }

    public BaseRuntimeException newInstance(String msgFormat) {
        return new BaseRuntimeException(this.code, msgFormat);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "BaseRuntimeException{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
