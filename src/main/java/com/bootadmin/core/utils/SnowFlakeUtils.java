package com.bootadmin.core.utils;

import com.github.yitter.contract.IIdGenerator;
import com.github.yitter.contract.IdGeneratorException;
import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.DefaultIdGenerator;
import org.springframework.stereotype.Component;

/**
 * @author kongzm
 * @description: 雪花算法
 * @date 2022/7/8 15:16
 */
@Component
public class SnowFlakeUtils {
    private static IIdGenerator idGenInstance = null;

    public static long nextId() throws IdGeneratorException {
        if (idGenInstance == null) {
            IdGeneratorOptions options = new IdGeneratorOptions((short) 1);
            options.WorkerIdBitLength = 1;
            idGenInstance = new DefaultIdGenerator(options);
        }

        return idGenInstance.newLong();
    }
}
