package com.bootadmin.core.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @description: 时间日期工具类
 */
public class DateUtils {
    public static final String DATE_FROMAT1 = "yyyy-MM-dd";
    public static final String DATE_FROMAT2 = "yyyy-MM-dd HH:mm:ss";
    /**
     * 当前时间是否在工作时间范围
     */
    public static SimpleDateFormat timeRangeDateFormat = new SimpleDateFormat("HH:mm");

    public static Date getDate(String s) {
        return getDate(s, null);
    }

    public static Date getJustDate(String s) {
        return getDate(s, "yyyy-MM-dd");
    }

    public static Date getDate(long date) {
        return getDate(date, null);
    }

    public static Date getJustDate(long date) {
        return getDate(date, "yyyy-MM-dd");
    }

    public static Date getDate(long date, String format) {
        if (StringUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        return getDate(formatDate(new Date(date), format), format);
    }

    public static Date getDate(String s, String format) {
        Date date;
        try {
            if (StringUtils.isEmpty(format)) {
                format = "yyyy-MM-dd HH:mm:ss";
            }
            date = new SimpleDateFormat(format).parse(s);
        } catch (Exception e) {
            date = new Date(0L);
        }
        return date;
    }

    public static String formatDate(long date, String format) {
        return formatDate(new Date(date), format);
    }

    public static String formatDate(long date) {
        return formatDate(new Date(date), null);
    }

    public static String formatJustDate(long date) {
        return formatDate(new Date(date), "yyyy-MM-dd");
    }

    public static String formatDate(Date date, String format) {
        if (StringUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        return new SimpleDateFormat(format).format(date);
    }


    /**
     * 获取一天的开始时间
     *
     * @return
     */
    public static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取一周的开始时间
     *
     * @return
     */
    public static Date getWeekStartTime() {
        Calendar weekStart = Calendar.getInstance();
        weekStart.set(weekStart.get(Calendar.YEAR), weekStart.get(Calendar.MONDAY), weekStart.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        weekStart.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return weekStart.getTime();
    }

    /**
     * 获取一个月的开始时间
     *
     * @return
     */
    public static Date getLast30Day() {
        Calendar curMonthStart = Calendar.getInstance();
        curMonthStart.add(Calendar.MONTH, 0);
        curMonthStart.set(Calendar.DAY_OF_MONTH, 1);
        curMonthStart.set(Calendar.HOUR_OF_DAY, 0);
        curMonthStart.set(Calendar.MINUTE, 0);
        curMonthStart.set(Calendar.SECOND, 0);
        curMonthStart.set(Calendar.MILLISECOND, 0);
        return curMonthStart.getTime();
    }

    /**
     * 获取一天的开始时间
     *
     * @return
     */
    public static Date getLastDay(int days) {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.DAY_OF_MONTH, -days);
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    /**
     * 获取一天的结束时间
     *
     * @return
     */
    public static Date getEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    /**
     * 获取一天的结束时间
     *
     * @return
     */
    public static Date getLastTime(int secs) {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.add(Calendar.SECOND, secs * -1);
        return todayEnd.getTime();
    }

    /**
     * 当前时间 是否为工作日
     *
     * @createTime：2017年11月24日 下午12:54:01
     */
    public static boolean isWeekDay(String bDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date bdate;
        try {
            bdate = format.parse(bDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(bdate);
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return true;
    }

    /*
     * 判断当前时间是否是周一
     */
    public static boolean isMonday(String bDate) {
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date bdate;
        try {
            bdate = format1.parse(bDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(bdate);
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * 检查当前时间是否是在 时间范围内 ，时间范围的格式为 ： 08:30~11:30,13:30~17:30
     *
     * @param timeRanges
     * @return
     */
    public static boolean isInWorkingHours(String timeRanges) {
        boolean workintTime = true;
        String timeStr = timeRangeDateFormat.format(new Date());
        if (!StringUtils.isBlank(timeRanges)) {        //设置了 工作时间段
            workintTime = false;                    //将 检查结果设置为 False ， 如果当前时间是在 时间范围内，则 置为 True
            String[] timeRange = timeRanges.split(",");
            for (String tr : timeRange) {
                String[] timeGroup = tr.split("~");
                if (timeGroup.length == 2) {
                    if (timeGroup[0].compareTo(timeGroup[1]) >= 0) {
                        if (timeStr.compareTo(timeGroup[0]) >= 0 || timeStr.compareTo(timeGroup[1]) <= 0) {
                            workintTime = true;
                        }
                    } else {
                        if (timeStr.compareTo(timeGroup[0]) >= 0 && timeStr.compareTo(timeGroup[1]) <= 0) {
                            workintTime = true;
                        }
                    }
                }
            }
        }
        return workintTime;
    }

}
