package com.bootadmin.core.vo;

import java.io.Serializable;

public interface VO extends Serializable
{
	public  String toString();
	public  String toJsonString();
}
