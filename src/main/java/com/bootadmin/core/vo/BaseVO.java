package com.bootadmin.core.vo;

import com.bootadmin.core.utils.JsonUtils;
import com.bootadmin.core.utils.StringUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseVO implements VO
{
	private static final long serialVersionUID = 1L;
	
    @Override
	public String toString()
	{
	  return StringUtils.toObjectString(this);
	}
	
    @Override
	public String toJsonString()
	{
	  return JsonUtils.toJsonString(this);
	}
	
	public static <T> T fromJsonString(String json, Class<T> cls)
	{
	  return JsonUtils.parseJavaObject(json, cls);
	}
}
