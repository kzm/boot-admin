package com.bootadmin.core.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author kongzm
 * @description: OpenApiConfig
 * @date 2022/1/13 14:39
 */
@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI springShopOpenAPI() {
        return new OpenAPI()
                .info(info());
    }

    private Info info() {
        return new Info()
                .title("BootAdmin Open API")
                .description("BootAdmin API List")
                .version("v0.0.1");
    }
}
