package com.bootadmin.core.config;

import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author kongzm
 * @description: 七牛配置
 * @date 2022/7/8 17:51
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "qiniu")
public class QiniuConfig {
    private String env;
    private String bucket;
    private String accessKey;
    private String secretKey;
    private String domain;

    @Bean
    public UploadManager uploadManager() {
        Configuration cfg = new Configuration(Region.region2());
        return new UploadManager(cfg);
    }
}
