package com.bootadmin.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

/*
 * @Description: BaseConfig
 * @Author: kzming
 * @Date: 2022-01-21 13:39:44
 */
@Configuration
@Getter
public class BaseConfig {
    @Value("${redis_prefix}")
    private String env;
}