package com.bootadmin.core.web;

/**
 * @author kongzm
 * @description: 基础url
 * @date 2022/1/13 11:02
 */
public class BaseUrl {
    public static final String BASE_URL = "/api/bootadmin";

    public static final String HOME = "/home";

    public static final String INSERT = "/insert";

    public static final String UPDATE = "/update";

    public static final String SELECT_PAGE_LIST = "/select-page-list";

    public static final String SELECT_LIST = "/select-list";

    public static final String SELECT_BY_ID = "/select-by-id";

    public static final String DELETE_BY_ID = "/delete-by-id";
}
