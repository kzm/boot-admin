package com.bootadmin.core.web;

/**
 * @author kongzm
 * @description: 系统常量
 * @date 2022/1/15 23:10
 */
public class SystemConstant {
    public static final int SUCCESS_CODE = 200;// 操作成功
    public static final int FAILURE_CODE = 10001;// 操作异常
    public static final int LOGIN_FAILURE_CODE = 40001; // 未登录
    public static final int AUTH_FAILURE_CODE = 40002; // 没有权限
    public static final int SYS_FAILURE_CODE = 500; // 系统异常

    public static final String JWT_SECERT = "3vG44tGUEHyhU0uhdw7sZrkvNY0jtwh3mSIklOwXpMONBZw=";		//密匙
}
