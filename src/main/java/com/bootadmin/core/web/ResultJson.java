package com.bootadmin.core.web;


import com.bootadmin.core.utils.StringUtils;

import java.io.Serializable;

/**
 * @description:web 返回结果封装
 */
public class ResultJson implements Serializable {

    private static final long serialVersionUID = 1L;

    private Object data;

    private String mess;

    private String result = SUCCESS_DESC;

    private int code = SUCCESS_CODE;

    private Long timeStamp = System.currentTimeMillis();

    private static final int SUCCESS_CODE = SystemConstant.SUCCESS_CODE;
    private static final String SUCCESS_DESC = "操作成功";

    private static final int FAILURE_CODE = SystemConstant.FAILURE_CODE;
    private static final String FAILURE_DESC = "操作异常";

    private static final int AUTH_FAILURE_CODE = SystemConstant.AUTH_FAILURE_CODE;
    private static final String AUTH_FAILURE_DESC = "没有权限";
    private static final int LOGIN_FAILURE_CODE = SystemConstant.LOGIN_FAILURE_CODE;
    private static final String LOGIN_FAILURE_DESC = "未登录";
    private static final int SYS_FAILURE_CODE = SystemConstant.SYS_FAILURE_CODE;
    private static final String SYS_FAILURE_DESC = "系统异常";

    public static ResultJson success(Object data, String mess, String result) {
        ResultJson resultJson = new ResultJson();
        resultJson.setData(data);
        resultJson.setMess(mess);
        resultJson.setResult(result);
        return resultJson;
    }

    public static ResultJson success() {
        return new ResultJson();
    }

    public static ResultJson success(Object data, String mess) {
        ResultJson responseJson = new ResultJson();
        responseJson.setData(data);
        responseJson.setMess(mess);
        responseJson.setCode(SUCCESS_CODE);
        return responseJson;
    }

    public static ResultJson success(Object data) {
        return success(data, SUCCESS_DESC);
    }

    public static ResultJson success(String mess) {
        ResultJson responseJson = new ResultJson();
        responseJson.setMess(StringUtils.isEmpty(mess) ? SUCCESS_DESC : mess);
        responseJson.setCode(SUCCESS_CODE);
        return responseJson;
    }

    public static ResultJson failure(String mess) {
        return error(FAILURE_CODE, FAILURE_DESC, mess);
    }

    public static ResultJson failureSys(String msg) {
        return error(SYS_FAILURE_CODE, SYS_FAILURE_DESC, msg);
    }

    public static ResultJson failureAuth() {
        return error(AUTH_FAILURE_CODE, AUTH_FAILURE_DESC, AUTH_FAILURE_DESC);
    }

    public static ResultJson failureLogin(String mess) {
        return error(LOGIN_FAILURE_CODE, LOGIN_FAILURE_DESC, mess);
    }

    public static ResultJson error(Integer code, String result, String msg) {
        ResultJson responseJson = new ResultJson();
        responseJson.setMess(msg);
        responseJson.setResult(result);
        responseJson.setCode(code);
        return responseJson;
    }

    public ResultJson() {
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ResultJson{" +
                "data=" + data +
                ", mess='" + mess + '\'' +
                ", result='" + result + '\'' +
                ", code=" + code +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
