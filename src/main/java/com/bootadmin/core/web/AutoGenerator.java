package com.bootadmin.core.web;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Property;
import com.bootadmin.core.utils.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author kongzm
 * @description: 自动生成代码
 * @date 2022/1/15 14:06
 */
@RestController
public class AutoGenerator {
    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String dbName;

    @Value("${spring.datasource.password}")
    private String dbPass;

    @GetMapping(value = "/auto-generator")
    public ResultJson autoGenerateor(String table) {
        if (StringUtils.isEmpty(table)) {
            return ResultJson.failure("表名不能为空");
        }

        ArrayList<String> tables = new ArrayList<>();

        if (table.contains(",")) {
            tables.addAll(Arrays.asList(table.split(",")));
        } else {
            tables.add(table);
        }

        FastAutoGenerator.create(url, dbName, dbPass)
            .globalConfig(builder -> {
                 builder.author("kzming")
                    .fileOverride()
                    .disableOpenDir()
                    // .enableSwagger()
                    .dateType(DateType.TIME_PACK)
                    .outputDir(System.getProperty("user.dir") + "/src/main/java");
            })
            .packageConfig(builder -> {
                builder.parent("com.bootadmin")
                    // .moduleName("sys")
                    .entity("entity")
                    .service("service")
                    .serviceImpl("service.impl")
                    .mapper("dao")
                    .xml("mapper")
                    .pathInfo(Collections.singletonMap(OutputFile.mapperXml, System.getProperty("user.dir") + "/src/main/resources/mybatis/mapper"));
            })
            .templateConfig(builder -> {
                builder.disable(TemplateType.CONTROLLER).entity("/templates/entity.java")
                    .service("/templates/service.java")
                    .serviceImpl("/templates/serviceImpl.java")
                    .mapper("/templates/mapper.java")
                    .mapperXml("/templates/mapper.xml");
            })
            .strategyConfig(builder -> {
                builder.addInclude(tables)
                .entityBuilder()
                .enableLombok()
                .enableActiveRecord()
                .columnNaming(NamingStrategy.underline_to_camel)
                .addTableFills(new Property("createTime", FieldFill.INSERT))
                .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))
                .addTableFills(new Property("lastAccess", FieldFill.INSERT_UPDATE))
                .versionColumnName("version")
                .idType(IdType.AUTO);
            }).strategyConfig(builder -> {
                builder.mapperBuilder().enableBaseResultMap().enableBaseColumnList().formatMapperFileName("%sDao");
            }).strategyConfig(builder -> {
                builder.serviceBuilder().formatServiceFileName("%sService");
            }).strategyConfig(builder -> {
                builder.controllerBuilder().enableRestStyle();
            })
            .templateEngine(new FreemarkerTemplateEngine())
            .execute();

        return ResultJson.success("table " + table + " is success.");
    }
}
