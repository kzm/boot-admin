package com.bootadmin.core.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author kongzm
 * @description: WebConfig
 * @date 2022/1/15 20:10
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private WebInterceptor webInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webInterceptor).addPathPatterns("/api/bootadmin/**")
        .excludePathPatterns(
                "/api/bootadmin/login",
                "/api/bootadmin/logout",
                "/api/bootadmin/reset-token",
                "/api/bootadmin/upload");

    }
}
