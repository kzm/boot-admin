package com.bootadmin.core.web;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author kongzm
 * @description: FileResponse
 * @date 2022/7/9 15:44
 */
@Getter
@Setter
public class FileResponse implements Serializable {
    private String fileUrl;
    private Long fileId;
    private Boolean success = true;
    private String msg = "上传成功";
}
