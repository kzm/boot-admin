/*
 * @Description: 会员登录信息
 * @Author: kzming
 * @Date: 2022-01-20 16:46:15
 */
package com.bootadmin.core.web;

import java.util.List;

import com.bootadmin.core.dto.BaseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseEmployeeLogin extends BaseDTO{
    private Long loginUid;
    private String username;
    private String realname;

    private Integer departmentManager;
    private Integer departmentDirector;

    private Long departmentId;
    private Boolean superAdmin;

    private List<Long> roles;        		        // 角色ID集合
    private List<String> allowedURIs;    			// 允许访问的URI列表
    private List<Long> allowedDepartmentIds;    	// 允许管理的部门列表
    private Long lastLoginTime;

    private String token;
}
