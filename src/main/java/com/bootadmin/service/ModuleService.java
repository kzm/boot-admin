package com.bootadmin.service;

import com.bootadmin.entity.Module;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统模块 服务类
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface ModuleService extends IService<Module> {

}
