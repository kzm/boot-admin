package com.bootadmin.service;

import com.bootadmin.entity.PermissionOperation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 权限-操作表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface PermissionOperationService extends IService<PermissionOperation> {

}
