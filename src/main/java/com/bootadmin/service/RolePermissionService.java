package com.bootadmin.service;

import com.bootadmin.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色-权限表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface RolePermissionService extends IService<RolePermission> {

}
