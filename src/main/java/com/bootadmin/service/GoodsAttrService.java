package com.bootadmin.service;

import com.bootadmin.entity.GoodsAttr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品属性表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsAttrService extends IService<GoodsAttr> {

}
