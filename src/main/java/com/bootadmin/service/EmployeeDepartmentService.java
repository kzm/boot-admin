package com.bootadmin.service;

import com.bootadmin.entity.EmployeeDepartment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 员工-部门表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface EmployeeDepartmentService extends IService<EmployeeDepartment> {

}
