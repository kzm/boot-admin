package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsAttr;
import com.bootadmin.dao.GoodsAttrDao;
import com.bootadmin.service.GoodsAttrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 商品属性表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsAttrServiceImpl extends ServiceImpl<GoodsAttrDao, GoodsAttr> implements GoodsAttrService {

}
