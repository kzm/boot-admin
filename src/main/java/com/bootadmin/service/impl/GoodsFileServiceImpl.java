package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsFile;
import com.bootadmin.dao.GoodsFileDao;
import com.bootadmin.service.GoodsFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 产品图片表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsFileServiceImpl extends ServiceImpl<GoodsFileDao, GoodsFile> implements GoodsFileService {

}
