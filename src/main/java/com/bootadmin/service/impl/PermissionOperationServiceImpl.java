package com.bootadmin.service.impl;

import com.bootadmin.entity.PermissionOperation;
import com.bootadmin.dao.PermissionOperationDao;
import com.bootadmin.service.PermissionOperationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 权限-操作表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class PermissionOperationServiceImpl extends ServiceImpl<PermissionOperationDao, PermissionOperation> implements PermissionOperationService {

}
