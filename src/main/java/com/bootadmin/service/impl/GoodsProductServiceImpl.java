package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsProduct;
import com.bootadmin.dao.GoodsProductDao;
import com.bootadmin.service.GoodsProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 货品表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsProductServiceImpl extends ServiceImpl<GoodsProductDao, GoodsProduct> implements GoodsProductService {

}
