package com.bootadmin.service.impl;

import com.bootadmin.entity.EmployeeDepartment;
import com.bootadmin.dao.EmployeeDepartmentDao;
import com.bootadmin.service.EmployeeDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 员工-部门表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class EmployeeDepartmentServiceImpl extends ServiceImpl<EmployeeDepartmentDao, EmployeeDepartment> implements EmployeeDepartmentService {

}
