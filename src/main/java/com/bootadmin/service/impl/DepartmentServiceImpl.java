package com.bootadmin.service.impl;

import com.bootadmin.entity.Department;
import com.bootadmin.dao.DepartmentDao;
import com.bootadmin.service.DepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 部门表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDao, Department> implements DepartmentService {

}
