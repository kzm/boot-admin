package com.bootadmin.service.impl;

import com.bootadmin.entity.RoleEmployee;
import com.bootadmin.dao.RoleEmployeeDao;
import com.bootadmin.service.RoleEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色-员工表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class RoleEmployeeServiceImpl extends ServiceImpl<RoleEmployeeDao, RoleEmployee> implements RoleEmployeeService {

}
