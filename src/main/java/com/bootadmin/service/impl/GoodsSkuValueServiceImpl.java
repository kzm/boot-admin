package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsSkuValue;
import com.bootadmin.dao.GoodsSkuValueDao;
import com.bootadmin.service.GoodsSkuValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * SKU-属性值关联表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsSkuValueServiceImpl extends ServiceImpl<GoodsSkuValueDao, GoodsSkuValue> implements GoodsSkuValueService {

}
