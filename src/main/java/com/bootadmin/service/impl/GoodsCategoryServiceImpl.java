package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsCategory;
import com.bootadmin.dao.GoodsCategoryDao;
import com.bootadmin.service.GoodsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 产品类目表 服务实现类
 *
 * @author kzming
 * @since 2022-07-10
 */
@Service
public class GoodsCategoryServiceImpl extends ServiceImpl<GoodsCategoryDao, GoodsCategory> implements GoodsCategoryService {

}
