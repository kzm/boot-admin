package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsAttrValue;
import com.bootadmin.dao.GoodsAttrValueDao;
import com.bootadmin.service.GoodsAttrValueService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 商品属性值表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsAttrValueServiceImpl extends ServiceImpl<GoodsAttrValueDao, GoodsAttrValue> implements GoodsAttrValueService {

}
