package com.bootadmin.service.impl;

import com.bootadmin.entity.Employee;
import com.bootadmin.dao.EmployeeDao;
import com.bootadmin.service.EmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 员工表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeDao, Employee> implements EmployeeService {

}
