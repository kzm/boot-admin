package com.bootadmin.service.impl;

import com.bootadmin.entity.Module;
import com.bootadmin.dao.ModuleDao;
import com.bootadmin.service.ModuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统模块 服务实现类
 *
 * @author kzming
 * @since 2022-01-15
 */
@Service
public class ModuleServiceImpl extends ServiceImpl<ModuleDao, Module> implements ModuleService {

}
