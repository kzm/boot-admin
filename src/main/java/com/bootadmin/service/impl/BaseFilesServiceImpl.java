package com.bootadmin.service.impl;

import com.bootadmin.entity.BaseFiles;
import com.bootadmin.dao.BaseFilesDao;
import com.bootadmin.service.BaseFilesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 *  服务实现类
 *
 * @author kzming
 * @since 2022-07-08
 */
@Service
public class BaseFilesServiceImpl extends ServiceImpl<BaseFilesDao, BaseFiles> implements BaseFilesService {

}
