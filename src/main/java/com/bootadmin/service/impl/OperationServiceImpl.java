package com.bootadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bootadmin.dao.OperationDao;
import com.bootadmin.entity.Operation;
import com.bootadmin.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 操作行为 服务实现类
 *
 * @author kzming
 * @since 2022-01-15
 */
@Service
public class OperationServiceImpl extends ServiceImpl<OperationDao, Operation> implements OperationService {

    @Autowired
    private OperationDao operationDao;

    @Override
    public List<String> getOperationListByEmployeeId(Long id) {
        List<Operation> list = operationDao.getOperationListByEmployeeId(id);
        return list == null ? null : list.stream().map(Operation::getUrl).collect(Collectors.toList());
    }

}
