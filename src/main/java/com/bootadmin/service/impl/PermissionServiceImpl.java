package com.bootadmin.service.impl;

import com.bootadmin.entity.Permission;
import com.bootadmin.dao.PermissionDao;
import com.bootadmin.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 操作行为 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, Permission> implements PermissionService {

}
