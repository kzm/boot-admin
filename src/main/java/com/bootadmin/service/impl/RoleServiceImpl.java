package com.bootadmin.service.impl;

import com.bootadmin.entity.Role;
import com.bootadmin.dao.RoleDao;
import com.bootadmin.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色表 服务实现类
 *
 * @author kzming
 * @since 2022-01-15
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService {

}
