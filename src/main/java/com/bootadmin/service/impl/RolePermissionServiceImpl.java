package com.bootadmin.service.impl;

import com.bootadmin.entity.RolePermission;
import com.bootadmin.dao.RolePermissionDao;
import com.bootadmin.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 角色-权限表 服务实现类
 *
 * @author kzming
 * @since 2022-01-17
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionDao, RolePermission> implements RolePermissionService {

}
