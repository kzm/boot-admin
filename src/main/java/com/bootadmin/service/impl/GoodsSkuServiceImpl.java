package com.bootadmin.service.impl;

import com.bootadmin.entity.GoodsSku;
import com.bootadmin.dao.GoodsSkuDao;
import com.bootadmin.service.GoodsSkuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * SKU表 服务实现类
 *
 * @author kzming
 * @since 2022-09-21
 */
@Service
public class GoodsSkuServiceImpl extends ServiceImpl<GoodsSkuDao, GoodsSku> implements GoodsSkuService {

}
