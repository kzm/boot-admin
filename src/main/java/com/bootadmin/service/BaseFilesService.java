package com.bootadmin.service;

import com.bootadmin.entity.BaseFiles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *  服务类
 *
 * @author kzming
 * @since 2022-07-08
 */
public interface BaseFilesService extends IService<BaseFiles> {

}
