package com.bootadmin.service;

import com.bootadmin.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色表 服务类
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface RoleService extends IService<Role> {

}
