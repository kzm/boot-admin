package com.bootadmin.service;

import com.bootadmin.entity.Operation;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 操作行为 服务类
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface OperationService extends IService<Operation> {
    /**
     * 获取员工操作权限列表
     * @param id
     * @return
     */
    List<String> getOperationListByEmployeeId(Long id);

}
