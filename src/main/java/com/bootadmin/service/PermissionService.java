package com.bootadmin.service;

import com.bootadmin.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 操作行为 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface PermissionService extends IService<Permission> {

}
