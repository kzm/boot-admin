package com.bootadmin.service;

import com.bootadmin.entity.GoodsSkuValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * SKU-属性值关联表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsSkuValueService extends IService<GoodsSkuValue> {

}
