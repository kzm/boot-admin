package com.bootadmin.service;

import com.bootadmin.entity.GoodsFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 产品图片表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsFileService extends IService<GoodsFile> {

}
