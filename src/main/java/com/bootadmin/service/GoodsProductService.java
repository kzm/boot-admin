package com.bootadmin.service;

import com.bootadmin.entity.GoodsProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 货品表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsProductService extends IService<GoodsProduct> {

}
