package com.bootadmin.service;

import com.bootadmin.entity.GoodsAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商品属性值表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsAttrValueService extends IService<GoodsAttrValue> {

}
