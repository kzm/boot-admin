package com.bootadmin.service;

import com.bootadmin.core.web.FileResponse;

import java.io.InputStream;

/**
 * @author kongzm
 * @description: 七牛服务
 * @date 2022/7/9 16:26
 */
public interface QiniuService {
    FileResponse upload(String projectName, String fileName, InputStream inputStream);
}
