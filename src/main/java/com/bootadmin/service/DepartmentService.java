package com.bootadmin.service;

import com.bootadmin.entity.Department;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 部门表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface DepartmentService extends IService<Department> {

}
