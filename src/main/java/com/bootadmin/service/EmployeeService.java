package com.bootadmin.service;

import com.bootadmin.entity.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 员工表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface EmployeeService extends IService<Employee> {

}
