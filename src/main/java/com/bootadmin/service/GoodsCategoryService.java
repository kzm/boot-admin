package com.bootadmin.service;

import com.bootadmin.entity.GoodsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 产品类目表 服务类
 *
 * @author kzming
 * @since 2022-07-10
 */
public interface GoodsCategoryService extends IService<GoodsCategory> {

}
