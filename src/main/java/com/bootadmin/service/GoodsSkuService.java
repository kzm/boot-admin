package com.bootadmin.service;

import com.bootadmin.entity.GoodsSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * SKU表 服务类
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsSkuService extends IService<GoodsSku> {

}
