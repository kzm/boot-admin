package com.bootadmin.service;

import com.bootadmin.entity.RoleEmployee;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色-员工表 服务类
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface RoleEmployeeService extends IService<RoleEmployee> {

}
