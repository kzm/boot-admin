package com.bootadmin.controller.system;

import com.bootadmin.controller.system.ao.EmployeeAO;
import com.bootadmin.controller.system.dto.request.EmployeePageListDTO;
import com.bootadmin.controller.system.dto.request.EmployeeSaveRequestDTO;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: EmployeeController
 * @date 2022/1/17 10:04
 */
@Tag(name = "员工管理")
@RestController
@RequestMapping(SystemUrl.EMPLOYEE)
public class EmployeeController {
    @Autowired
    private EmployeeAO employeeAO;

    @PostMapping(SystemUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody EmployeeSaveRequestDTO employeeSaveRequestDTO) {
        return employeeAO.save(request, employeeSaveRequestDTO);
    }

    @PostMapping(SystemUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody EmployeeSaveRequestDTO employeeSaveRequestDTO) {
        return employeeAO.save(request, employeeSaveRequestDTO);
    }

    @PostMapping(SystemUrl.DELETE_BY_ID)
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        return employeeAO.deleteById(request, id);
    }

    @GetMapping(SystemUrl.SELECT_PAGE_LIST)
    public ResultJson selectPageList(EmployeePageListDTO employeePageListDTO) {
        return employeeAO.selectPageList(employeePageListDTO);
    }
}
