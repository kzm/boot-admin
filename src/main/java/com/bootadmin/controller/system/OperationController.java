package com.bootadmin.controller.system;

import com.bootadmin.controller.system.ao.OperationAO;
import com.bootadmin.controller.system.dto.request.OperationPageListDTO;
import com.bootadmin.controller.system.dto.request.OperationSaveRequestDTO;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 操作行为 前端控制器
 *
 * @author kzming
 * @since 2022-01-15
 */
@RestController
@Tag(name = "操作类")
@RequestMapping(SystemUrl.OPERATION)
public class OperationController {
    @Autowired
    private OperationAO operationAO;


    @PostMapping(SystemUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody OperationSaveRequestDTO operationSaveRequestDTO) {
        return operationAO.save(request, operationSaveRequestDTO);
    }

    @PostMapping(SystemUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody OperationSaveRequestDTO operationSaveRequestDTO) {
        return operationAO.save(request, operationSaveRequestDTO);
    }

    @PostMapping(SystemUrl.DELETE_BY_ID)
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        return operationAO.deleteById(request, id);
    }

    @GetMapping(SystemUrl.SELECT_PAGE_LIST)
    public ResultJson selectPageList(OperationPageListDTO operationPageListDTO) {
        return operationAO.selectPageList(operationPageListDTO);
    }
}
