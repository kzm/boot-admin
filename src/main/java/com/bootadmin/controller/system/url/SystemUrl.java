package com.bootadmin.controller.system.url;

import com.bootadmin.core.web.BaseUrl;

/**
 * @author kongzm
 * @description: SystemUrl
 * @date 2022/1/13 11:04
 */
public class SystemUrl extends BaseUrl {
    // 系统
    public final static String SYSTEM = BASE_URL + "/system";
    // 操作菜单
    public static final String OPERATION = BASE_URL + "/operation";
    // 模块菜单
    public static final String MODULE = BASE_URL + "/module";
    // 角色菜单
    public static final String ROLE = BASE_URL + "/role";
    // 员工菜单
    public static final String EMPLOYEE = BASE_URL + "/employee";
    // 部门菜单
    public static final String DEPARTMENT = BASE_URL + "/department";
    // 权限菜单
    public static final String PERMISSION = BASE_URL + "/permission";
}
