package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: 操作
 * @date 2022/3/10 15:46
 */
@Getter
@Setter
public class OperationPageListDTO extends BaseDTO {
    private String name;
    private String url;
    private String moduleCode;
    private Integer pageNum = 1;
    private Integer pageSize = 15;
}
