package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: 操作
 * @date 2022/3/10 15:45
 */
@Getter
@Setter
public class OperationSaveRequestDTO extends BaseDTO {
    private Long id;
    private String moduleCode;
    private String name;
    private String url;
}
