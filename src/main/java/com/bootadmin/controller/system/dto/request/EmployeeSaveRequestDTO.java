package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: EmployeeSaveRequestDTO
 * @date 2022/1/17 10:09
 */
@Getter
@Setter
public class EmployeeSaveRequestDTO extends BaseDTO {
    private Long id;
    private String username;
    private String realname;
    private String password;
    private String mobile;
    private Long departmentId;
    private Integer departmentManager;
    private Integer departmentDirector;
    private Integer state;
    private Integer surper;
}
