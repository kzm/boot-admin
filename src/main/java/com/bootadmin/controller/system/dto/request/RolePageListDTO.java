package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: RolePageListDTO
 * @date 2022/3/10 15:34
 */
@Getter
@Setter
public class RolePageListDTO extends BaseDTO {
    private String name;

    private Integer pageSize = 15;

    private Integer pageNum = 1;
}
