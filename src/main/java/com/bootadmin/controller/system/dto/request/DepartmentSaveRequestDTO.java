package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: DepartmentSaveRequestDTO
 * @date 2022/1/17 14:04
 */
@Setter
@Getter
public class DepartmentSaveRequestDTO extends BaseDTO {
    private Long id;
    private Long pid;
    private String name;
    private String code;
    private Integer state;
}
