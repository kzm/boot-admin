package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: EmployeePageListDTO
 * @date 2022/3/10 14:27
 */
@Getter
@Setter
public class EmployeePageListDTO extends BaseDTO {
    private Long departmentId;

    private String name;

    private Integer state;

    private Integer pageSize = 15;

    private Integer pageNum = 1;
}
