/*
 * @Description: 
 * @Author: kzming
 * @Date: 2022-01-20 11:47:13
 */
package com.bootadmin.controller.system.dto.request;

import com.bootadmin.core.dto.BaseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDTO extends BaseDTO {
    private String username;
    private String password;
}
