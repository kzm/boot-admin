package com.bootadmin.controller.system;

import com.bootadmin.controller.system.ao.ModuleAO;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统模块 前端控制器
 *
 * @author kzming
 * @since 2022-01-15
 */
@RestController
@Tag(name = "模块类")
@RequestMapping(SystemUrl.MODULE)
public class ModuleController {

    @Autowired
    private ModuleAO moduleAO;

    @Operation(summary = "新增")
    @PostMapping(value = SystemUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, String code, String name) {
        return moduleAO.insert(request, code, name);
    }

    @Operation(summary = "编辑")
    @PostMapping(value = SystemUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, Long id, String name) {
        return moduleAO.update(request, id, name);
    }

    @PostMapping(SystemUrl.DELETE_BY_ID)
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        return moduleAO.deleteById(request, id);
    }

    /**
     * @Description 查询所有模块
     * @Author kongzm
     * @Date 2022/3/10 14:04
     * @Param [name]
     * @return com.bootadmin.core.web.ResultJson
     **/
    @GetMapping(SystemUrl.SELECT_LIST)
    public ResultJson selectList() {
        return moduleAO.selectList();
    }
}
