package com.bootadmin.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.Module;
import com.bootadmin.service.ModuleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(SystemUrl.SYSTEM)
@Tag(name = "系统模块")
class SystemController {
    @Autowired
    ModuleService moduleService;

    @Operation(summary = "首页")
    @GetMapping(value = SystemUrl.HOME)
    public ResultJson index() {
        return ResultJson.success("system home is ok");
    }

    @GetMapping(value = "/test")
    public ResultJson test() {
        IPage<Module> page = new Page<>(1, 15);

        QueryWrapper<Module> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("code", "25");

        IPage<Module> list = moduleService.page(page, queryWrapper);
        return ResultJson.success(list);
    }
}