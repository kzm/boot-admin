package com.bootadmin.controller.system.ao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bootadmin.controller.system.ao.EmployeeAO;
import com.bootadmin.controller.system.dto.request.EmployeePageListDTO;
import com.bootadmin.controller.system.dto.request.EmployeeSaveRequestDTO;
import com.bootadmin.core.utils.StringUtils;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.Employee;
import com.bootadmin.enums.StateEnum;
import com.bootadmin.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: EmployeeAOImpl
 * @date 2022/1/17 10:07
 */
@Component
public class EmployeeAOImpl implements EmployeeAO {
    @Autowired
    private EmployeeService employeeService;

    @Override
    public ResultJson save(HttpServletRequest request, EmployeeSaveRequestDTO employeeSaveRequestDTO) {
        Employee employee = new Employee();

        if (employeeSaveRequestDTO.getId() == null) {
            if (StringUtils.isTrimEmpty(employeeSaveRequestDTO.getPassword())) {
                return ResultJson.failure("密码不能为空");
            }
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            employeeSaveRequestDTO.setPassword(bCryptPasswordEncoder.encode(employeeSaveRequestDTO.getPassword()));
        } else {
            Employee em = employeeService.getById(employeeSaveRequestDTO.getId());
            employee.setVersion(em.getVersion());
            if (!StringUtils.isEmpty(employeeSaveRequestDTO.getPassword())) {
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
                employeeSaveRequestDTO.setPassword(bCryptPasswordEncoder.encode(employeeSaveRequestDTO.getPassword()));
            } else {
                employeeSaveRequestDTO.setPassword(em.getPassword());
            }
        }

        BeanUtils.copyProperties(employeeSaveRequestDTO, employee);
        employeeService.saveOrUpdate(employee);

        return ResultJson.success("ok");
    }

    @Override
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        Employee employee = employeeService.getById(id);
        int state = employee.getState().equals(StateEnum.ONE.getValue()) ? StateEnum.ZERO.getValue() : StateEnum.ONE.getValue();
        employee.setState(state);
        employeeService.updateById(employee);
        return ResultJson.success();
    }

    @Override
    public ResultJson selectPageList(EmployeePageListDTO employeePageListDTO) {
        QueryWrapper<Employee> wrapper = new QueryWrapper<>();
        if (!StringUtils.isTrimEmpty(employeePageListDTO.getName())) {
            wrapper.like("realname", employeePageListDTO.getName().trim());
        }
        if (employeePageListDTO.getDepartmentId() != null) {
            wrapper.eq("department_id", employeePageListDTO.getDepartmentId());
        }
        if (employeePageListDTO.getState() != null) {
            wrapper.eq("state", employeePageListDTO.getState());
        }

        wrapper.orderByDesc("id");

        Page<Employee> page = new Page<>(employeePageListDTO.getPageNum(), employeePageListDTO.getPageSize());
        IPage<Employee> list = employeeService.page(page, wrapper);
        return ResultJson.success(list);
    }
}
