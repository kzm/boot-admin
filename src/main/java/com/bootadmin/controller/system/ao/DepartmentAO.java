package com.bootadmin.controller.system.ao;

import com.bootadmin.controller.system.dto.request.DepartmentSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: DepartmentAO
 * @date 2022/1/17 14:03
 */
public interface DepartmentAO {
    /**
     * @Description 新增、保存
     * @Author kongzm
     * @Date 2022/1/17 14:09
     * @Param [request, departmentSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, DepartmentSaveRequestDTO departmentSaveRequestDTO);

    /**
     * @Description 禁用部门
     * @Author kongzm
     * @Date 2022/3/10 13:48
     * @Param [request, id]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson deleteById(HttpServletRequest request, Long id);

    /**
     * @Description 查询所有部门
     * @Author kongzm
     * @Date 2022/3/10 14:28
     * @Param []
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectList();
}
