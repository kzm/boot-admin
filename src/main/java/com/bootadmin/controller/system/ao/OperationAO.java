package com.bootadmin.controller.system.ao;

import com.bootadmin.controller.system.dto.request.OperationPageListDTO;
import com.bootadmin.controller.system.dto.request.OperationSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description 操作ao
 * @Author kongzm
 * @Date 2022/3/10 15:42
 **/
public interface OperationAO {
    /**
     * @Description 新增、保存
     * @Author kongzm
     * @Date 2022/3/10 15:45
     * @Param [request, operationSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, OperationSaveRequestDTO operationSaveRequestDTO);

    /**
     * @Description 删
     * @Author kongzm
     * @Date 2022/3/10 15:46
     * @Param [request, id]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson deleteById(HttpServletRequest request, Long id);

    /**
     * @Description 查
     * @Author kongzm
     * @Date 2022/3/10 15:46
     * @Param [employeePageListDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectPageList(OperationPageListDTO operationPageListDTO);
}
