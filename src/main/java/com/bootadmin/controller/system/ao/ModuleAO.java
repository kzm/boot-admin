package com.bootadmin.controller.system.ao;

import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

public interface ModuleAO {
    /**
     * @Description 新增模块
     * @Author kongzm
     * @Date 2022/1/14 20:42
     * @Param []
     * @return com.bootadmin.core.web.ResultJson
     *@param request
     * @param code
     * @param name   */
    public ResultJson insert(HttpServletRequest request, String code, String name);

    /**
     * @Description 编辑
     * @Author kongzm
     * @Date 2022/1/17 9:38
     * @Param [request, id, name]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson update(HttpServletRequest request, Long id, String name);

    /**
     * @Description 删
     * @Author kongzm
     * @Date 2022/3/10 16:02
     * @Param
     * @return
     **/
    ResultJson deleteById(HttpServletRequest request, Long id);

    /**
     * @Description 查询所有
     * @Author kongzm
     * @Date 2022/3/10 16:02
     * @Param []
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectList();
}
