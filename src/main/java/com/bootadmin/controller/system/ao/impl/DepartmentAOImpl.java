package com.bootadmin.controller.system.ao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bootadmin.controller.system.ao.DepartmentAO;
import com.bootadmin.controller.system.dto.request.DepartmentSaveRequestDTO;
import com.bootadmin.core.utils.StringUtils;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.Department;
import com.bootadmin.enums.StateEnum;
import com.bootadmin.service.DepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author kongzm
 * @description: DepartmentAO
 * @date 2022/1/17 14:03
 */
@Component
public class DepartmentAOImpl implements DepartmentAO {
    @Autowired
    DepartmentService departmentService;

    @Override
    public ResultJson save(HttpServletRequest request, DepartmentSaveRequestDTO departmentSaveRequestDTO) {
        Department department = new Department();
        if (departmentSaveRequestDTO.getId() == null) {
            String newCode = this.createCode(departmentSaveRequestDTO.getCode());
            departmentSaveRequestDTO.setCode(newCode);
        } else {
            Department dep = departmentService.getById(departmentSaveRequestDTO.getId());
            department.setVersion(dep.getVersion());
        }

        BeanUtils.copyProperties(departmentSaveRequestDTO, department);
        departmentService.saveOrUpdate(department);

        return ResultJson.success("保存成功");
    }

    @Override
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        Department depart = departmentService.getById(id);
        int state = depart.getState().equals(StateEnum.ONE.getValue()) ? StateEnum.ZERO.getValue() : StateEnum.ONE.getValue();
        depart.setState(state);
        departmentService.updateById(depart);
        return ResultJson.success();
    }

    @Override
    public ResultJson selectList() {
        List<Department> list = departmentService.list();
        return ResultJson.success(list);
    }

    // 生成code
    private String createCode(String code) {
        boolean codeIsEmpty = false;
        QueryWrapper<Department> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isEmpty(code)) {
            queryWrapper.eq("LENGTH(code)", 3);
            codeIsEmpty = true;
        } else {
            queryWrapper.eq("LENGTH(code)", code.length() + 3);
            queryWrapper.likeRight("code", code);
        }

        queryWrapper.orderByDesc("id").last("limit 1");
        Department res = departmentService.getOne(queryWrapper);

        if (res == null && codeIsEmpty) {
            return "001";
        } else if (res != null){
            int c = Integer.parseInt(res.getCode().substring(res.getCode().length() - 3)) + 1;
            return (codeIsEmpty ? "" : code) + String.format("%03d", c);
        } else {
            return code + "001";
        }
    }
}
