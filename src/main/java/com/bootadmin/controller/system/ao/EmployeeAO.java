package com.bootadmin.controller.system.ao;

import com.bootadmin.controller.system.dto.request.EmployeePageListDTO;
import com.bootadmin.controller.system.dto.request.EmployeeSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: EmployeeAO
 * @date 2022/1/17 10:06
 */
public interface EmployeeAO {
    /**
     * @Description 新增、编辑员工
     * @Author kongzm
     * @Date 2022/1/17 10:10
     * @Param [request, employeeSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, EmployeeSaveRequestDTO employeeSaveRequestDTO);

    /**
     * @Description 离职
     * @Author kongzm
     * @Date 2022/3/10 14:00
     * @Param [request, id]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson deleteById(HttpServletRequest request, Long id);

    /**
     * @Description 员工列表
     * @Author kongzm
     * @Date 2022/3/10 14:13
     * @Param [departmentId, name, state]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectPageList(EmployeePageListDTO employeePageListDTO);
}
