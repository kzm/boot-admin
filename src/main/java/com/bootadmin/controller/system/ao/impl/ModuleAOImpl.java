package com.bootadmin.controller.system.ao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bootadmin.controller.system.ao.ModuleAO;
import com.bootadmin.core.utils.StringUtils;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.Module;
import com.bootadmin.enums.StateEnum;
import com.bootadmin.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author kongzm
 * @description: ModuleAOImpl
 * @date 2022/1/14 20:42
 */
@Component
public class ModuleAOImpl implements ModuleAO {
    @Autowired
    private ModuleService moduleService;

    @Override
    public ResultJson insert(HttpServletRequest request, String code, String name) {
        if (StringUtils.isEmpty(name)) {
            return ResultJson.failure("模块名称不能为空");
        }

        Module insertModule = new Module();
        String newCode = createModuleCode(code);
        insertModule.setCode(newCode);
        insertModule.setName(name);
        moduleService.save(insertModule);
        return ResultJson.success("保存成功");
    }

    @Override
    public ResultJson update(HttpServletRequest request, Long id, String name) {
        if (id == null || StringUtils.isEmpty(name)) {
            return ResultJson.failure("参数有误");
        }

        Module module = moduleService.getById(id);
        if (module != null) {
            module.setName(name);
            moduleService.updateById(module);
        }

        return ResultJson.success("保存成功");
    }

    @Override
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        Module module = moduleService.getById(id);
        int state = module.getState().equals(StateEnum.ONE.getValue()) ? StateEnum.ZERO.getValue() : StateEnum.ONE.getValue();
        module.setState(state);
        moduleService.updateById(module);
        return ResultJson.success();
    }

    @Override
    public ResultJson selectList() {
        List<Module> list = moduleService.list();
        return ResultJson.success(list);
    }

    // 生成code
    private String createModuleCode(String code) {
        boolean codeIsEmpty = false;
        QueryWrapper<Module> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isEmpty(code)) {
            queryWrapper.eq("LENGTH(code)", 2);
            codeIsEmpty = true;
        } else {
            queryWrapper.eq("LENGTH(code)", code.length() + 2);
            queryWrapper.likeRight("code", code);
        }

        queryWrapper.orderByDesc("id").last("limit 1");
        Module res = moduleService.getOne(queryWrapper);
        if (res == null && codeIsEmpty) {
            return "001";
        } else if (res != null) {
            int c = Integer.parseInt(res.getCode().substring(res.getCode().length() - 2)) + 1;
            return (codeIsEmpty ? "" : code) + String.format("%02d", c);
        } else {
            return code + "001";
        }
    }
}
