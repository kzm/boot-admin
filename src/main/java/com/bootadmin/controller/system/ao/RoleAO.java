package com.bootadmin.controller.system.ao;

import com.bootadmin.controller.system.dto.request.RolePageListDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

public interface RoleAO {
    /**
     * @Description 新增/保存
     * @Author kongzm
     * @Date 2022/1/15 19:38
     * @Param [request, id, name, remark]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, Long id, String name, String remark);

    /**
     * @Description 删除角色
     * @Author kongzm
     * @Date 2022/3/10 15:26
     * @Param [request, id]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson deleteById(HttpServletRequest request, Long id);

    /**
     * @Description 分页列表
     * @Author kongzm
     * @Date 2022/3/10 15:40
     * @Param [rolePageListDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectPageList(RolePageListDTO rolePageListDTO);
}
