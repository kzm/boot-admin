package com.bootadmin.controller.system;

import com.bootadmin.controller.system.ao.DepartmentAO;
import com.bootadmin.controller.system.dto.request.DepartmentSaveRequestDTO;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: EmployeeController
 * @date 2022/1/17 10:04
 */
@Tag(name = "部门管理")
@RestController
@RequestMapping(SystemUrl.DEPARTMENT)
public class DepartmentController {
    @Autowired
    private DepartmentAO departmentAO;

    @PostMapping(SystemUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody DepartmentSaveRequestDTO departmentSaveRequestDTO) {
        return departmentAO.save(request, departmentSaveRequestDTO);
    }

    @PostMapping(SystemUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody DepartmentSaveRequestDTO departmentSaveRequestDTO) {
        return departmentAO.save(request, departmentSaveRequestDTO);
    }

    @PostMapping(SystemUrl.DELETE_BY_ID)
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        return departmentAO.deleteById(request, id);
    }
    
    /**
     * @Description 查询所有部门
     * @Author kongzm
     * @Date 2022/3/10 14:04
     * @Param [name]
     * @return com.bootadmin.core.web.ResultJson
     **/
    @GetMapping(SystemUrl.SELECT_LIST)
    public ResultJson selectList() {
        return departmentAO.selectList();
    }
}
