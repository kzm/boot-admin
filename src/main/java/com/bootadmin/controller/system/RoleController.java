package com.bootadmin.controller.system;

import com.bootadmin.controller.system.ao.RoleAO;
import com.bootadmin.controller.system.dto.request.RolePageListDTO;
import com.bootadmin.controller.system.url.SystemUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 角色表 前端控制器
 *
 * @author kzming
 * @since 2022-01-15
 */
@RestController
@RequestMapping(SystemUrl.ROLE)
public class RoleController {

    @Autowired
    private RoleAO roleAO;

    @Operation(summary = "新增")
    @PostMapping(value = SystemUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, String name, String remark) {
        return roleAO.save(request, null, name, remark);
    }

    @PostMapping(SystemUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, Long id, String name, String remark) {
        return roleAO.save(request, id, name, remark);
    }

    @PostMapping(SystemUrl.DELETE_BY_ID)
    public ResultJson deleteById(HttpServletRequest request, Long id) {
        return roleAO.deleteById(request, id);
    }

    @GetMapping(SystemUrl.SELECT_PAGE_LIST)
    public ResultJson selectPageList(RolePageListDTO rolePageListDTO) {
        return roleAO.selectPageList(rolePageListDTO);
    }
}
