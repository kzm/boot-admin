package com.bootadmin.controller.goods.urls;

import com.bootadmin.core.web.BaseUrl;

/**
 * @author kongzm
 * @description: 产品url
 * @date 2022/7/10 16:51
 */
public class ProductUrl extends BaseUrl {
    public static final String PRODUCT = BASE_URL + "/product";
}
