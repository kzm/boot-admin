package com.bootadmin.controller.goods.urls;

import com.bootadmin.core.web.BaseUrl;

/**
 * @author kongzm
 * @description: 分类url
 * @date 2022/7/10 16:51
 */
public class AttrUrl extends BaseUrl {
    public static final String ATTR = BASE_URL + "/attr";
}
