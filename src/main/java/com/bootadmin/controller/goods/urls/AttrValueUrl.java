package com.bootadmin.controller.goods.urls;

import com.bootadmin.core.web.BaseUrl;

/**
 * @author kongzm
 * @description: 分类url
 * @date 2022/7/10 16:51
 */
public class AttrValueUrl extends BaseUrl {
    public static final String ATTR_VALUE = BASE_URL + "/attr-value";
}
