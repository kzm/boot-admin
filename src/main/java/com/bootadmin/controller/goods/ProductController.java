package com.bootadmin.controller.goods;

import com.bootadmin.controller.goods.ao.ProductAO;
import com.bootadmin.controller.goods.dto.GoodsProductSaveRequestDTO;
import com.bootadmin.controller.goods.urls.ProductUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: 产品控制器
 * @date 2022/7/10 16:49
 */
@RestController
@Tag(name = "产品控制器")
@RequestMapping(ProductUrl.PRODUCT)
public class ProductController {
    @Autowired
    private ProductAO productAO;

    @PostMapping(ProductUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody GoodsProductSaveRequestDTO goodsProductSaveRequestDTO) {
        return productAO.save(request, goodsProductSaveRequestDTO);
    }

    @PostMapping(ProductUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody GoodsProductSaveRequestDTO goodsProductSaveRequestDTO) {
        return productAO.save(request, goodsProductSaveRequestDTO);
    }

    @GetMapping(ProductUrl.SELECT_BY_ID)
    public ResultJson selectById(HttpServletRequest request, Long id) {
        return productAO.selectById(request, id);
    }
}
