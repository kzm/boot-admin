package com.bootadmin.controller.goods.ao.impl;

import com.bootadmin.controller.goods.ao.ProductAO;
import com.bootadmin.controller.goods.dto.GoodsProductSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.GoodsFile;
import com.bootadmin.entity.GoodsProduct;
import com.bootadmin.entity.GoodsSku;
import com.bootadmin.service.GoodsFileService;
import com.bootadmin.service.GoodsProductService;
import com.bootadmin.service.GoodsSkuService;
import com.bootadmin.service.GoodsSkuValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: ProductAOImpl
 * @date 2022/7/10 16:54
 */
@Component
public class ProductAOImpl implements ProductAO {
    private final Logger logger = LoggerFactory.getLogger(ProductAOImpl.class);

    @Resource
    private GoodsProductService goodsProductService;
    @Resource
    private GoodsSkuService goodsSkuService;
    @Resource
    private GoodsSkuValueService goodsSkuValueService;
    @Resource
    private GoodsFileService goodsFileService;

    @Override
    @Transactional
    public ResultJson save(HttpServletRequest request, GoodsProductSaveRequestDTO goodsProductSaveRequestDTO) {
        System.out.println(goodsProductSaveRequestDTO);

        GoodsProduct goodsProduct = new GoodsProduct();

        // 保存产品
        BeanUtils.copyProperties(goodsProductSaveRequestDTO, goodsProduct);
        goodsProductService.save(goodsProduct);

        // 保存产品附件
        if (goodsProductSaveRequestDTO.getFileList() != null && goodsProductSaveRequestDTO.getFileList().size() > 0) {
            goodsProductSaveRequestDTO.getFileList().forEach(item -> {
                GoodsFile goodsFile = new GoodsFile();
                BeanUtils.copyProperties(item, goodsFile);
                goodsFile.setRelationId(goodsProduct.getId());
                goodsFileService.save(goodsFile);
            });
        }

        // 保存sku信息
        goodsProductSaveRequestDTO.getSkuList().forEach(item -> {
            GoodsSku goodsSku = new GoodsSku();
            BeanUtils.copyProperties(item, goodsSku);
            goodsSku.setProductId(goodsProduct.getId());
            goodsSku.setProductName(goodsProduct.getName());

        });


        return ResultJson.success("保存成功");
    }

    @Override
    public ResultJson selectById(HttpServletRequest request, Long id) {
        return null;
    }

}
