package com.bootadmin.controller.goods.ao;

import com.bootadmin.controller.goods.dto.GoodsCategorySaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description 分类
 * @Author kongzm
 * @Date 2022/7/10 16:53
 **/
public interface CategoryAO {
    /**
     * @Description 新增、编辑
     * @Author kongzm
     * @Date 2022/7/10 17:18
     * @Param [request, goodsCategorySaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, GoodsCategorySaveRequestDTO goodsCategorySaveRequestDTO);
    /**
     * @Description 列表
     * @Author kongzm
     * @Date 2022/7/10 17:18
     * @Param [request]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectList(HttpServletRequest request);
}
