package com.bootadmin.controller.goods.ao;

import com.bootadmin.controller.goods.dto.GoodsProductSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description 产品
 * @Author kongzm
 * @Date 2022/7/10 16:53
 **/
public interface ProductAO {
    /**
     * @Description 新增、编辑
     * @Author kongzm
     * @Date 2022/7/10 17:18
     * @Param [request, goodsProductSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, GoodsProductSaveRequestDTO goodsProductSaveRequestDTO);

    /**
     * @Description 查询详情
     * @Author kongzm
     * @Date 2022/9/21 12:17
     * @Param [request, id]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectById(HttpServletRequest request, Long id);
}
