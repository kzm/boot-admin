package com.bootadmin.controller.goods.ao.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bootadmin.controller.goods.ao.AttrAO;
import com.bootadmin.controller.goods.dto.GoodsAttrSaveRequestDTO;
import com.bootadmin.controller.goods.dto.GoodsAttrValueSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.GoodsAttr;
import com.bootadmin.entity.GoodsAttrValue;
import com.bootadmin.service.GoodsAttrService;
import com.bootadmin.service.GoodsAttrValueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author kongzm
 * @description: AttrAOImpl
 * @date 2022/7/10 16:54
 */
@Component
public class AttrAOImpl implements AttrAO {
    private final Logger logger = LoggerFactory.getLogger(AttrAOImpl.class);

    @Autowired
    private GoodsAttrService goodsAttrService;
    @Autowired
    private GoodsAttrValueService goodsAttrValueService;

    @Override
    public ResultJson save(HttpServletRequest request, GoodsAttrSaveRequestDTO goodsAttrSaveRequestDTO) {
        GoodsAttr goodsAttr = new GoodsAttr();
        if (goodsAttrSaveRequestDTO.getId() != null) {
            goodsAttr = goodsAttrService.getById(goodsAttrSaveRequestDTO.getId());
            goodsAttr.setVersion(goodsAttr.getVersion() + 1);
        } else {
            QueryWrapper<GoodsAttr> wrapper = new QueryWrapper<>();
            wrapper.eq("attr_name", goodsAttrSaveRequestDTO.getAttrName());
            GoodsAttr one = goodsAttrService.getOne(wrapper);
            if (one != null) {
                return ResultJson.success(one);
            }
        }

        BeanUtils.copyProperties(goodsAttrSaveRequestDTO, goodsAttr);
        goodsAttrService.saveOrUpdate(goodsAttr);

        return ResultJson.success(goodsAttr);
    }

    @Override
    public ResultJson selectList(HttpServletRequest request) {
        List<GoodsAttr> list = goodsAttrService.list();
        return ResultJson.success(list);
    }

    @Override
    public ResultJson saveValue(HttpServletRequest request, GoodsAttrValueSaveRequestDTO goodsAttrValueSaveRequestDTO) {
        GoodsAttrValue goodsAttrValue = new GoodsAttrValue();
        if (goodsAttrValueSaveRequestDTO.getId() != null) {
            goodsAttrValue = goodsAttrValueService.getById(goodsAttrValueSaveRequestDTO.getId());
            goodsAttrValue.setVersion(goodsAttrValue.getVersion() + 1);
        } else {
            QueryWrapper<GoodsAttrValue> wrapper = new QueryWrapper<>();
            wrapper.eq("attr_value", goodsAttrValueSaveRequestDTO.getAttrValue());
            GoodsAttrValue one = goodsAttrValueService.getOne(wrapper);
            if (one != null) {
                return ResultJson.success(one);
            }
        }

        BeanUtils.copyProperties(goodsAttrValueSaveRequestDTO, goodsAttrValue);
        goodsAttrValueService.saveOrUpdate(goodsAttrValue);

        return ResultJson.success(goodsAttrValue);
    }

    @Override
    public ResultJson selectValueList(HttpServletRequest request) {
        List<GoodsAttrValue> list = goodsAttrValueService.list();
        return ResultJson.success(list);
    }
}
