package com.bootadmin.controller.goods.ao;

import com.bootadmin.controller.goods.dto.GoodsAttrSaveRequestDTO;
import com.bootadmin.controller.goods.dto.GoodsAttrValueSaveRequestDTO;
import com.bootadmin.core.web.ResultJson;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description 属性
 * @Author kongzm
 * @Date 2022/7/10 16:53
 **/
public interface AttrAO {
    /**
     * @Description 新增、编辑
     * @Author kongzm
     * @Date 2022/7/10 17:18
     * @Param [request, goodsAttrSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson save(HttpServletRequest request, GoodsAttrSaveRequestDTO goodsAttrSaveRequestDTO);
    /**
     * @Description 列表
     * @Author kongzm
     * @Date 2022/7/10 17:18
     * @Param [request]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectList(HttpServletRequest request);

    /**
     * @Description 属性值保存
     * @Author kongzm
     * @Date 2022/9/21 11:52
     * @Param [request, goodsAttrSaveRequestDTO]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson saveValue(HttpServletRequest request, GoodsAttrValueSaveRequestDTO goodsAttrSaveRequestDTO);

    /**
     * @Description 属性值列表
     * @Author kongzm
     * @Date 2022/9/21 11:52
     * @Param [request]
     * @return com.bootadmin.core.web.ResultJson
     **/
    ResultJson selectValueList(HttpServletRequest request);
}
