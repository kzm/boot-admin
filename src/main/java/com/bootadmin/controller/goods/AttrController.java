package com.bootadmin.controller.goods;

import com.bootadmin.controller.goods.ao.AttrAO;
import com.bootadmin.controller.goods.dto.GoodsAttrSaveRequestDTO;
import com.bootadmin.controller.goods.urls.AttrUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: 属性控制器
 * @date 2022/7/10 16:49
 */
@RestController
@Tag(name = "属性控制器")
@RequestMapping(AttrUrl.ATTR)
public class AttrController {
    @Autowired
    private AttrAO attrAO;

    @PostMapping(AttrUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody GoodsAttrSaveRequestDTO goodsAttrSaveRequestDTO) {
        return attrAO.save(request, goodsAttrSaveRequestDTO);
    }

    @PostMapping(AttrUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody GoodsAttrSaveRequestDTO goodsAttrSaveRequestDTO) {
        return attrAO.save(request, goodsAttrSaveRequestDTO);
    }

    @GetMapping(AttrUrl.SELECT_LIST)
    public ResultJson selectList(HttpServletRequest request) {
        return attrAO.selectList(request);
    }
}
