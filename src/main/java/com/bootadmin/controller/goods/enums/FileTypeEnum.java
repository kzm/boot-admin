package com.bootadmin.controller.goods.enums;

import lombok.Getter;

@Getter
public enum FileTypeEnum {
    ONE(1, "产品图片"),
    TWO(2, "SKU图片");

    private final Integer value;

    private final String desc;

    FileTypeEnum(Integer value, String desc) {
        this.desc = desc;
        this.value = value;
    }

    public static FileTypeEnum getEnum(int value) {
        FileTypeEnum resultEnum = null;
        FileTypeEnum[] enumAry = FileTypeEnum.values();

        for (FileTypeEnum fileTypeEnum : enumAry) {
            if (fileTypeEnum.getValue() == value) {
                resultEnum = fileTypeEnum;
                break;
            }
        }

        return resultEnum;
    }
}
