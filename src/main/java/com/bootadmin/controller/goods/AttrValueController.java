package com.bootadmin.controller.goods;

import com.bootadmin.controller.goods.ao.AttrAO;
import com.bootadmin.controller.goods.dto.GoodsAttrValueSaveRequestDTO;
import com.bootadmin.controller.goods.urls.AttrValueUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: 属性值控制器
 * @date 2022/7/10 16:49
 */
@RestController
@Tag(name = "属性值控制器")
@RequestMapping(AttrValueUrl.ATTR_VALUE)
public class AttrValueController {
    @Autowired
    private AttrAO attrAO;

    @PostMapping(AttrValueUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody GoodsAttrValueSaveRequestDTO goodsAttrSaveRequestDTO) {
        return attrAO.saveValue(request, goodsAttrSaveRequestDTO);
    }

    @PostMapping(AttrValueUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody GoodsAttrValueSaveRequestDTO goodsAttrSaveRequestDTO) {
        return attrAO.saveValue(request, goodsAttrSaveRequestDTO);
    }

    @GetMapping(AttrValueUrl.SELECT_LIST)
    public ResultJson selectList(HttpServletRequest request) {
        return attrAO.selectValueList(request);
    }
}
