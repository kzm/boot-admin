package com.bootadmin.controller.goods;

import com.bootadmin.controller.goods.ao.CategoryAO;
import com.bootadmin.controller.goods.dto.GoodsCategorySaveRequestDTO;
import com.bootadmin.controller.goods.urls.CateUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author kongzm
 * @description: 分类模块
 * @date 2022/7/10 16:49
 */
@RestController
@Tag(name = "分类")
@RequestMapping(CateUrl.CATEGORY)
public class CategoryController {
    @Autowired
    private CategoryAO categoryAO;

    @PostMapping(CateUrl.INSERT)
    public ResultJson insert(HttpServletRequest request, @RequestBody GoodsCategorySaveRequestDTO goodsCategorySaveRequestDTO) {
        return categoryAO.save(request, goodsCategorySaveRequestDTO);
    }

    @PostMapping(CateUrl.UPDATE)
    public ResultJson update(HttpServletRequest request, @RequestBody GoodsCategorySaveRequestDTO goodsCategorySaveRequestDTO) {
        return categoryAO.save(request, goodsCategorySaveRequestDTO);
    }

    @GetMapping(CateUrl.SELECT_LIST)
    public ResultJson selectList(HttpServletRequest request) {
        return categoryAO.selectList(request);
    }
}
