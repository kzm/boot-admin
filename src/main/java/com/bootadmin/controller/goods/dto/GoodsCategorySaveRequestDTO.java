package com.bootadmin.controller.goods.dto;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: GoodsCategorySaveRequestDTO
 * @date 2022/7/10 17:15
 */
@Getter
@Setter
public class GoodsCategorySaveRequestDTO extends BaseDTO {
    private Long id;
    private Long pid;
    private String name;
    private String level;
    private Integer state;
}
