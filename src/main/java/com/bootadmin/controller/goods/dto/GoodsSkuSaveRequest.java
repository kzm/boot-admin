package com.bootadmin.controller.goods.dto;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author kongzm
 * @description: GoodsSkuSaveRequest
 * @date 2022/9/21 12:25
 */
@Getter
@Setter
public class GoodsSkuSaveRequest extends BaseDTO {
    private BigDecimal price;
    private String skuNumber;
    private List<Long> valueList;
    private List<GoodsFileSaveRequest> fileList;
}
