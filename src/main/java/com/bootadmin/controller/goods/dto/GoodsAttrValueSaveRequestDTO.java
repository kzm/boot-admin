package com.bootadmin.controller.goods.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: GoodsAttrValueSaveRequestDTO
 * @date 2022/9/21 11:51
 */
@Setter
@Getter
public class GoodsAttrValueSaveRequestDTO {
    private Long id;
    private Long attrId;
    private String attrValue;
}
