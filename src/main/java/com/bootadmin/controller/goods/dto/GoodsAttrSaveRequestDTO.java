package com.bootadmin.controller.goods.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author kongzm
 * @description: GoodsAttrSaveRequestDTO
 * @date 2022/9/21 11:03
 */
@Getter
@Setter
public class GoodsAttrSaveRequestDTO implements Serializable {
    private Long id;
    private String attrName;
    private String aliasName;
}
