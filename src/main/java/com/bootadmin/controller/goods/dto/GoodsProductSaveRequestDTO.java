package com.bootadmin.controller.goods.dto;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author kongzm
 * @description: GoodsProductSaveRequestDTO
 * @date 2022/9/21 12:18
 */
@Getter
@Setter
public class GoodsProductSaveRequestDTO extends BaseDTO {
    private Long id;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 型号
     */
    private String modelNumber;

    /**
     * 分类id
     */
    private Long categoryId;

    /**
     * 父级分类id
     */
    private Long categoryPid;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 规格
     */
    private String spec;

    /**
     * 描述
     */
    private String description;

    /**
     * 备注
     */
    private String remark;

    // sku明细
    List<GoodsSkuSaveRequest> skuList;
    // 产品图册
    List<GoodsFileSaveRequest> fileList;
}
