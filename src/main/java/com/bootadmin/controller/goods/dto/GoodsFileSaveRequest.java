package com.bootadmin.controller.goods.dto;

import com.bootadmin.core.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: GoodFileSaveRequest
 * @date 2022/9/21 12:28
 */
@Getter
@Setter
public class GoodsFileSaveRequest extends BaseDTO {
    private Integer fileType;
    private Long fileId;
    private String fileUrl;
}
