package com.bootadmin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bootadmin.controller.system.dto.request.LoginDTO;
import com.bootadmin.core.config.BaseConfig;
import com.bootadmin.core.utils.JwtUtils;
import com.bootadmin.core.utils.RedisUtil;
import com.bootadmin.core.utils.StringUtils;
import com.bootadmin.core.web.BaseEmployeeLogin;
import com.bootadmin.core.web.ResultJson;
import com.bootadmin.entity.Employee;
import com.bootadmin.entity.EmployeeDepartment;
import com.bootadmin.entity.RoleEmployee;
import com.bootadmin.enums.BooleanEnum;
import com.bootadmin.enums.StateEnum;
import com.bootadmin.service.EmployeeDepartmentService;
import com.bootadmin.service.EmployeeService;
import com.bootadmin.service.OperationService;
import com.bootadmin.service.RoleEmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author kongzm
 * @description: 登陆
 * @date 2022/1/15 22:10
 */
@RestController
@Tag(name = "登陆")
@RequestMapping("/api/bootadmin")
public class LoginController {

    private final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private RoleEmployeeService roleEmployeeService;
    @Autowired
    private EmployeeDepartmentService employeeDepartmentService;
    @Autowired
    private OperationService operationService;
    @Resource
    RedisUtil redisUtil;
    @Autowired
    BaseConfig baseConfig;

    @PostMapping("login")
    @Operation(summary = "登陆验证")
    public ResultJson login(HttpServletRequest request, @RequestBody LoginDTO loginDTO) {
        logger.debug(request.getContentType());
        if (!StringUtils.isEmpty(loginDTO.getUsername())) {
            QueryWrapper<Employee> queryWrapper = new QueryWrapper<Employee>();
            queryWrapper.eq("username", loginDTO.getUsername());
            Employee employee = employeeService.getOne(queryWrapper);
            if (employee == null) {
                return ResultJson.failure("用户不存在");
            }

            if (employee.getState().equals(BooleanEnum.NO.getValue())) {
                return ResultJson.failure("用户已禁用");
            }

            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            if (!bCryptPasswordEncoder.matches(loginDTO.getPassword(), employee.getPassword())) {
                return ResultJson.failure("密码错误");
            }

            BaseEmployeeLogin employeeLogin = new BaseEmployeeLogin();
            BeanUtils.copyProperties(employee, employeeLogin);
            employeeLogin.setLoginUid(employee.getId());
            employeeLogin.setSuperAdmin(employee.getSurper().equals(1));

            if (!employeeLogin.getSuperAdmin()) {
                // 角色
                QueryWrapper<RoleEmployee> roleQuery = new QueryWrapper<RoleEmployee>();
                roleQuery.eq("employee_id", employee.getId());
                roleQuery.eq("state", StateEnum.ONE.getValue());
                List<RoleEmployee> roleList = roleEmployeeService.list(roleQuery);
                List<Long> roles = new ArrayList<>();
                if (roleList != null) {
                    roles = roleList.stream().map(RoleEmployee::getRoleId).collect(Collectors.toList());
                    employeeLogin.setRoles(roles);
                }
                // 部门权限
                QueryWrapper<EmployeeDepartment> departmentQuery = new QueryWrapper<EmployeeDepartment>();
                departmentQuery.eq("employee_id", employee.getId());
                departmentQuery.eq("state", StateEnum.ONE.getValue());
                List<EmployeeDepartment> deprtmentList = employeeDepartmentService.list(departmentQuery);
                if (deprtmentList != null) {
                    List<Long> departmentIds = deprtmentList.stream().map(EmployeeDepartment::getDepartmentId).collect(Collectors.toList());
                    departmentIds.add(employee.getDepartmentId());
                    employeeLogin.setAllowedDepartmentIds(departmentIds);
                }
    
                // 操作集合-存储到redis
                if (roleList != null) {
                    this.setOperationList(employee.getId());
                }
            }

            try {
                String token = JwtUtils.createJWT(employeeLogin);
                employeeLogin.setToken(token);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultJson.failure(e.getMessage());
            }

            return ResultJson.success(employeeLogin);
        }

        return ResultJson.failure("用户名不能为空");
    }

    // 存储到redis
    private void setOperationList(Long id) {
        List<String> list = operationService.getOperationListByEmployeeId(id);
        String key = baseConfig.getEnv() + id.toString() + "URI";
        if (list != null) {
            redisUtil.set(key, list);
        }
    }

    @PostMapping("logout")
    @Operation(summary = "登出")
    public ResultJson logout() {
        return ResultJson.success();
    }
}
