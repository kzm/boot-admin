package com.bootadmin.controller.dto;

import com.bootadmin.core.vo.BaseVO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kongzm
 * @description: FileVO
 * @date 2022/10/12 10:04
 */
@Getter
@Setter
public class FileVO extends BaseVO {
    private Long fileId;
    private String fileUrl;
}
