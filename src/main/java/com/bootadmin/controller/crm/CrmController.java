package com.bootadmin.controller.crm;

import com.bootadmin.controller.crm.url.CrmUrl;
import com.bootadmin.core.web.ResultJson;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "客户管理")
@RequestMapping(CrmUrl.CRM)
class CrmController {
    @Operation(summary = "首页")
    @GetMapping(value = CrmUrl.HOME)
    public ResultJson index() {
        return ResultJson.success("crm home is ok");
    }
}