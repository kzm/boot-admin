package com.bootadmin.controller.crm.url;

import com.bootadmin.core.web.BaseUrl;

/**
 * @author kongzm
 * @description: CRMUrl
 * @date 2022/1/14 15:05
 */
public class CrmUrl extends BaseUrl {

    public final static String CRM = BASE_URL + "/crm";
    // 首页
    public static final String HOME = "/home";
}
