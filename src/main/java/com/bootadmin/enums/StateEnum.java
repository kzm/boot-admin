/*
 * @Description: 状态枚举
 * @Author: kzming
 * @Date: 2022-01-20 17:41:17
 */
package com.bootadmin.enums;

import lombok.Getter;

@Getter
public enum StateEnum {
    ONE(1, "是"),
    ZERO(0, "否");

    private final Integer value;

    private final String desc;

    StateEnum(Integer value, String desc) {
        this.desc = desc;
        this.value = value;
    }

    public static StateEnum getEnum(int value) {
        StateEnum resultEnum = null;
        StateEnum[] enumAry = StateEnum.values();

        for (StateEnum stateEnum : enumAry) {
            if (stateEnum.getValue() == value) {
                resultEnum = stateEnum;
                break;
            }
        }

        return resultEnum;
    }
}
