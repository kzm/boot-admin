package com.bootadmin.enums;

import lombok.Getter;

/*
 * @Description: 是否枚举
 * @Author: kzming
 * @Date: 2022-01-20 17:21:42
 */
@Getter
public enum BooleanEnum {
    YES(1, "是"),
    NO(0, "否");

    private final Integer value;

    private final String desc;

    BooleanEnum(Integer value, String desc) {
        this.desc = desc;
        this.value = value;
    }

    public static BooleanEnum getEnum(int value) {
        BooleanEnum resultEnum = null;
        BooleanEnum[] enumAry = BooleanEnum.values();

        for (BooleanEnum booleanEnum : enumAry) {
            if (booleanEnum.getValue() == value) {
                resultEnum = booleanEnum;
                break;
            }
        }

        return resultEnum;
    }
}
