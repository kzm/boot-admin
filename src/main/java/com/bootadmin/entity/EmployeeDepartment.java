package com.bootadmin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 员工-部门表
 *
 * @author kzming
 * @since 2022-01-17
 */
@Getter
@Setter
@ToString
@TableName("employee_department")
public class EmployeeDepartment extends Model<EmployeeDepartment> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 员工id
     */
    private Long employeeId;

    /**
     * 部门id
     */
    private Long departmentId;

    /**
     * 是否禁用 1-正常 0-禁用
     */
    private Integer state;

    /**
     * 编辑人id
     */
    private Long adminId;

    /**
     * 编辑人名称
     */
    private String adminName;

    /**
     * 最后编辑时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long lastAccess;

    /**
     * 版本号
     */
    @Version
    private Integer version;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
