package com.bootadmin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 商品属性值表
 *
 * @author kzming
 * @since 2022-09-21
 */
@Getter
@Setter
@ToString
@TableName("goods_attr_value")
public class GoodsAttrValue extends Model<GoodsAttrValue> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 属性id
     */
    private Long attrId;

    /**
     * 属性值
     */
    private String attrValue;

    /**
     * 是否禁用 1-正常 0-禁用
     */
    private Integer state;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**
     * 最后编辑时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long lastAccess;

    /**
     * 版本号
     */
    @Version
    private Integer version;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
