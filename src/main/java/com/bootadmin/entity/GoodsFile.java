package com.bootadmin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 产品图片表
 *
 * @author kzming
 * @since 2022-09-21
 */
@Getter
@Setter
@ToString
@TableName("goods_file")
public class GoodsFile extends Model<GoodsFile> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 关联数据主键
     */
    private Long relationId;

    /**
     * 文件类型 1-产品图片 2-sku图片
     */
    private Integer fileType;

    /**
     * 文件id
     */
    private Long fileId;

    /**
     * 文件地址
     */
    private String fileUrl;

    /**
     * 是否禁用 1-正常 0-禁用
     */
    private Integer state;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**
     * 最后编辑时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long lastAccess;

    /**
     * 版本号
     */
    @Version
    private Integer version;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
