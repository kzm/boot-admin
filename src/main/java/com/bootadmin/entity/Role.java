package com.bootadmin.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 角色表
 *
 * @author kzming
 * @since 2022-01-17
 */
@Getter
@Setter
@ToString
public class Role extends Model<Role> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色说明
     */
    private String remark;

    /**
     * 是否禁用 1-正常 0-禁用
     */
    private Integer state;

    /**
     * 创建人
     */
    private Long createId;

    /**
     * 创建人姓名
     */
    private String createName;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /**
     * 更新人
     */
    private Long updateId;

    /**
     * 更新人姓名
     */
    private String updateName;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    /**
     * 最后编辑时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long lastAccess;

    /**
     * 版本号
     */
    @Version
    private Integer version;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
