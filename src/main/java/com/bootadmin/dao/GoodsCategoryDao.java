package com.bootadmin.dao;

import com.bootadmin.entity.GoodsCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 产品类目表 Mapper 接口
 *
 * @author kzming
 * @since 2022-07-10
 */
public interface GoodsCategoryDao extends BaseMapper<GoodsCategory> {

}
