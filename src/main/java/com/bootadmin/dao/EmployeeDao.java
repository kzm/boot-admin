package com.bootadmin.dao;

import com.bootadmin.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 员工表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface EmployeeDao extends BaseMapper<Employee> {

}
