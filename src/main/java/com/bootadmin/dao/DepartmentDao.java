package com.bootadmin.dao;

import com.bootadmin.entity.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 部门表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface DepartmentDao extends BaseMapper<Department> {

}
