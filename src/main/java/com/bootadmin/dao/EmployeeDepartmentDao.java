package com.bootadmin.dao;

import com.bootadmin.entity.EmployeeDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 员工-部门表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface EmployeeDepartmentDao extends BaseMapper<EmployeeDepartment> {

}
