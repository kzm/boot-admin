package com.bootadmin.dao;

import com.bootadmin.entity.BaseFiles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper 接口
 *
 * @author kzming
 * @since 2022-07-08
 */
public interface BaseFilesDao extends BaseMapper<BaseFiles> {

}
