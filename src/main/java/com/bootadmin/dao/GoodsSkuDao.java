package com.bootadmin.dao;

import com.bootadmin.entity.GoodsSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * SKU表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsSkuDao extends BaseMapper<GoodsSku> {

}
