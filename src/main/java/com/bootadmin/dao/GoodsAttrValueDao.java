package com.bootadmin.dao;

import com.bootadmin.entity.GoodsAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品属性值表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsAttrValueDao extends BaseMapper<GoodsAttrValue> {

}
