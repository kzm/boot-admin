package com.bootadmin.dao;

import com.bootadmin.entity.GoodsSkuValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * SKU-属性值关联表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsSkuValueDao extends BaseMapper<GoodsSkuValue> {

}
