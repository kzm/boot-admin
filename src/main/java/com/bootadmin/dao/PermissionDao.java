package com.bootadmin.dao;

import com.bootadmin.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 操作行为 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface PermissionDao extends BaseMapper<Permission> {

}
