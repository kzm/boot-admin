package com.bootadmin.dao;

import com.bootadmin.entity.RoleEmployee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色-员工表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface RoleEmployeeDao extends BaseMapper<RoleEmployee> {

}
