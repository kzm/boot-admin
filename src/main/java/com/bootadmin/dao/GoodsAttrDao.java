package com.bootadmin.dao;

import com.bootadmin.entity.GoodsAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 商品属性表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsAttrDao extends BaseMapper<GoodsAttr> {

}
