package com.bootadmin.dao;

import com.bootadmin.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface RoleDao extends BaseMapper<Role> {

}
