package com.bootadmin.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bootadmin.entity.Operation;

/**
 * 操作行为 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface OperationDao extends BaseMapper<Operation> {
    /**
     * 查询操作权限
     * @param id
     * @return
     */
    List<Operation> getOperationListByEmployeeId(Long id);

}
