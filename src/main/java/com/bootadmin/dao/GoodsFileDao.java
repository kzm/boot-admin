package com.bootadmin.dao;

import com.bootadmin.entity.GoodsFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 产品图片表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsFileDao extends BaseMapper<GoodsFile> {

}
