package com.bootadmin.dao;

import com.bootadmin.entity.GoodsProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 货品表 Mapper 接口
 *
 * @author kzming
 * @since 2022-09-21
 */
public interface GoodsProductDao extends BaseMapper<GoodsProduct> {

}
