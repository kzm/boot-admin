package com.bootadmin.dao;

import com.bootadmin.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色-权限表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface RolePermissionDao extends BaseMapper<RolePermission> {

}
