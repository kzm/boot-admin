package com.bootadmin.dao;

import com.bootadmin.entity.PermissionOperation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 权限-操作表 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-17
 */
public interface PermissionOperationDao extends BaseMapper<PermissionOperation> {

}
