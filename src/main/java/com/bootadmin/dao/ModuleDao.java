package com.bootadmin.dao;

import com.bootadmin.entity.Module;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 系统模块 Mapper 接口
 *
 * @author kzming
 * @since 2022-01-15
 */
public interface ModuleDao extends BaseMapper<Module> {

}
